using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace rz_report
{
    public class Opcodes
    {
        Rizin rizin;
        string path;

        public Opcodes(Rizin rizin, string path)
        {
            this.rizin = rizin;
            this.path = $"{path}/opcodes";
            Directory.CreateDirectory(this.path);
        }

        // ! disassembles section containing the first entrypoint
        public void Disassemble()
        {
            using (DataTable dt1 = rizin.CommandDataTable("iej"))
            using (DataTable dt2 = rizin.CommandDataTable("iSj"))
            {
                foreach (var dr_entry in dt1.Rows
                    .OfType<DataRow>()
                    .Where(x => x[".[].vaddr"] != DBNull.Value))
                {
                    foreach (var dr in dt2.Rows
                        .OfType<DataRow>()
                        .Select((x, i) => new { x, i })
                        .Where(g => g.x[".[].perm"] != DBNull.Value))
                    {
                        string name, perm;
                        decimal vaddr, vsize, vaddr_entry;
                        if (TryGetValue(dr.x, ".[].name", out name) &&
                            TryGetValue(dr.x, ".[].perm", out perm) &&
                            TryGetValue(dr.x, ".[].vaddr", out vaddr) &&
                            TryGetValue(dr.x, ".[].vsize", out vsize) &&
                            TryGetValue(dr_entry, ".[].vaddr", out vaddr_entry) &&
                            vaddr <= vaddr_entry &&
                            vaddr_entry < vaddr + vsize)
                        {
                            using (var stream = new FileStream($"{path}/{dr.i}_{perm}_{name}.txt", FileMode.Create, FileAccess.Write, FileShare.Read))
                            {
                                DisassembleSection(vaddr, vsize, stream);
                                stream.WriteByte(10);
                            }
                            using (var stream = new FileStream($"{path}/{dr.i}_{perm}_{name}.json", FileMode.Create, FileAccess.Write, FileShare.Read))
                            {
                                DisassembleSectionJson(vaddr, vsize, stream);
                            }
                        }
                    }
                }
            }
        }

        private static bool TryGetValue<T>(DataRow dr, string col, out T value)
        {
            if (dr[col] == DBNull.Value)
            {
                value = default(T);
                return false;
            }
            value = (T)dr[col];
            return true;
        }

        private void DisassembleSection(decimal vaddr, decimal vsize, Stream stream)
        {
            string colors = rizin.CommandString("e scr.color");
            string lines = rizin.CommandString("e asm.lines");
            rizin.Command("e scr.color=0");
            rizin.Command("e asm.lines=false");
            string opcodes = rizin.CommandString($"pD {vsize} @ {vaddr}");
            rizin.Command($"e scr.color={colors}");
            rizin.Command($"e asm.lines={lines}");
            byte[] data = Encoding.UTF8.GetBytes(opcodes);
            stream.Write(data, 0, data.Length);
        }

        private void DisassembleSectionJson(decimal vaddr, decimal vsize, Stream stream)
        {
            string json = rizin.CommandString($"pDj {vsize} @ {vaddr}");
            byte[] data = Encoding.UTF8.GetBytes(json);
            stream.Write(data, 0, data.Length);
        }
    }
}