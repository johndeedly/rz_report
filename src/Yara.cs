using System;
using System.Buffers;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using toolbelt;

namespace rz_report
{
    public class Yara
    {
        Rizin rizin;
        string basePath;
        string origName;

        public Yara(Rizin rizin, string basePath, string origName)
        {
            this.rizin = rizin;
            this.basePath = basePath;
            this.origName = origName;
        }

        private IEnumerable<Tuple<string, string>> YaraRuleList(out IEnumerable<string> imports)
        {
            Console.WriteLine($"Generating YARA ruleset");
            IEnumerable<string> result = FindAllYaraRuleFiles();
            result = SortRuleFiles(result);
            IEnumerable<Tuple<string, string>> filesToRules = MapRuleFilesToContainingRules(result, out imports);
            filesToRules = WithoutDuplicateRules(filesToRules);
            Console.WriteLine($"YARA ruleset written to \"{basePath}/{origName} - yara.yara\"");
            return filesToRules;
        }

        private static IEnumerable<Tuple<string, string>> WithoutDuplicateRules(IEnumerable<Tuple<string, string>> filesToRules)
        {
            List<Tuple<string, string>> rules = new();
            HashSet<string> set = new();
            int duplicates = 0;
            foreach (var tuple in filesToRules)
            {
                if (set.Add(tuple.Item1))
                    rules.Add(tuple);
                else
                    duplicates++;
            }
            if (duplicates > 0)
                Console.WriteLine($"skipped {duplicates} duplicate yara rules");
            return rules;
        }

        private static IEnumerable<Tuple<string, string>> MapRuleFilesToContainingRules(IEnumerable<string> result, out IEnumerable<string> imports)
        {
            List<Tuple<string, string>> list = new();
            HashSet<string> imps = new();
            Regex removeComments = new Regex(@"(?<=\s|^)/\*.*?\*/", RegexOptions.Compiled | RegexOptions.Multiline | RegexOptions.Singleline);
            // (?<!//[^\n]*)(private )?rule\s+  all rules that are not inside an inline comment
            // (?<name>[^\s\{:]+)               filter out the rule name for "uniqueness" later on
            // [^\n]*(\{|\n\{)                  include tags, spaces, newlines etc. until you hit "{"
            // .*?(?<!//[^\n]*)condition:       match all metas and strings until the start of condition (watch out for inline comments)
            // (?<condition>.*?).*?\}           conditions don't contain curly brackets
            Regex regexYara = new Regex(@"(?<!//[^\n]*)(private )?rule\s+(?<name>[^\s\{:]+)[^\n]*(\{|\n\{).*?(?<!//[^\n]*)condition:(?<condition>.*?)\}", RegexOptions.Compiled | RegexOptions.Singleline);
            Regex regexFilterImports = new Regex(@"^import\s"".*""", RegexOptions.Compiled | RegexOptions.Multiline);
            Regex regexFilterConditions = new Regex(@"filename|filepath|extension|owner", RegexOptions.Compiled | RegexOptions.Singleline);
            foreach (var file in result)
            {
                string commentableFilename = file.Replace("*/", "_/");
                string txt = File.ReadAllText(file);
                txt = removeComments.Replace(txt, string.Empty);
                foreach (Match match in regexYara.Matches(txt))
                {
                    if (match.Success)
                    {
                        if (!regexFilterConditions.IsMatch(match.Groups["condition"].Value))
                        {
                            list.Add(Tuple.Create(match.Groups["name"].Value, $"/*\n\t{commentableFilename}\n*/\n{match.Value}"));
                        }
                    }
                }
                foreach (Match match in regexFilterImports.Matches(txt))
                {
                    if (match.Success)
                    {
                        if (!regexFilterConditions.IsMatch(match.Value))
                        {
                            imps.Add(match.Value.Trim());
                        }
                    }
                }
            }
            imports = (IEnumerable<string>)imps;
            return list;
        }

        private static IEnumerable<string> SortRuleFiles(IEnumerable<string> result)
        {
            result = result.OrderBy(x => Path.GetFileName(x));
            return result;
        }

        private IEnumerable<string> FindAllYaraRuleFiles()
        {
            IEnumerable<string> result = Enumerable.Empty<string>();
            if (Directory.Exists("rules"))
            {
                result = result.Concat(Directory
                    .EnumerateFiles("rules", "*.yar*", SearchOption.AllDirectories)
                    .Where(x => x.EndsWith(".yar") || x.EndsWith(".yara")));
            }
            string assemblyRules = Path.GetDirectoryName(new Uri(Assembly.GetExecutingAssembly().Location).LocalPath) + "/rules";
            if (Directory.Exists(assemblyRules))
            {
                result = result.Concat(Directory
                    .EnumerateFiles(assemblyRules, "*.yar*", SearchOption.AllDirectories)
                    .Where(x => x.EndsWith(".yar") || x.EndsWith(".yara")));
            }
            return result;
        }

        public void TryYaraCheck()
        {
            var outputBuffer = new ArrayBufferWriter<byte>();
            using (var jsonWriter = new Utf8JsonWriter(outputBuffer))
            {
                jsonWriter.WriteStartArray();

                IterateMatches(jsonWriter);

                jsonWriter.WriteEndArray();
            }

            var enc = new UTF8Encoding(false);
            string json = enc.GetString(outputBuffer.WrittenSpan);
            File.WriteAllText($"{basePath}/{origName} - yara.json", json, enc);

            using (var stream1 = new FileStream($"{basePath}/{origName} - yara.csv", FileMode.Create, FileAccess.Write, FileShare.Read))
            using (var stream2 = new FileStream($"{basePath}/{origName} - yara.xlsx", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            using (DataTable dt = new DataTable())
            {
                JsonDocument jdoc = JsonDocument.Parse(json);
                dt.FromJson(jdoc.RootElement);
                dt.ToCsv(stream1).ToOxml(stream2);
            }
        }

        private void IterateMatches(Utf8JsonWriter jsonWriter)
        {
            rizin.Command("fs yara");
            rizin.Command("fss+ yara");
            try
            {
                IEnumerable<string> imports;
                IEnumerable<Tuple<string, string>> rules = YaraRuleList(out imports);
                if (!rules.Any())
                    return;
                Console.WriteLine($"scanning with {rules.Count()} unique yara rules");
                string yars = "/*\n\tmerged YARA rules for rz_report\n*/\n\n" +
                    string.Join("\n", imports) +
                    "\n\n" +
                    string.Join("\n\n", rules.Select(x => x.Item2));
                File.WriteAllText($"{basePath}/{origName} - yara.yara", yars);

                string filePath = null;
                using (var json = rizin.CommandJson("ij"))
                    filePath = json.RootElement.GetProperty("core").GetProperty("file").GetString();
                if (string.IsNullOrWhiteSpace(filePath))
                    return;

                Console.WriteLine($"Beginning YARA scan");
                string result = ShellUtils.RunShellTextAsync("yara", $"-s -L -e -w \"{basePath}/{origName} - yara.yara\" \"{filePath}\"").Await();
                Console.WriteLine($"Finished YARA scan");
                
                using (var sr = new StringReader(result))
                {
                    int cnt = 0;
                    string line, name = null;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.StartsWith("default:"))
                        {
                            if (name != null)
                            {
                                jsonWriter.WriteEndArray();
                                jsonWriter.WriteEndObject();
                            }
                            name = Regex.Match(line, @"default:(.*?)\s")?.Groups[1]?.Value;
                            jsonWriter.WriteStartObject();
                            jsonWriter.WriteString("match", name);
                            Console.WriteLine($"Yara hit \"{name}\"");
                            jsonWriter.WriteStartArray("hits");
                        }
                        else if (line.StartsWith("0x") && !string.IsNullOrWhiteSpace(name))
                        {
                            Match match = Regex.Match(line, @"(0x[a-f0-9]+)(:[0-9]+)?(:.*?)?[:\s]");
                            if (match.Success)
                            {
                                decimal offset;
                                string length, identifier, mark;
                                ParseMatch(name, match, out offset, out length, out identifier, out mark);

                                decimal? mappedOffset = MapYaraToRizinOffset(offset);
                                string rawdata = null;
                                string rawascii = null;
                                if (mappedOffset.HasValue)
                                {
                                    GetRawData(length, mappedOffset, out rawdata, out rawascii);

                                    MarkInsideRizin(cnt, name, offset, length, identifier, mark, mappedOffset);
                                }

                                WriteJson(jsonWriter, offset, length, identifier, rawdata, rawascii);

                                cnt++;
                            }
                        }
                    }

                    if (name != null)
                    {
                        jsonWriter.WriteEndArray();
                        jsonWriter.WriteEndObject();
                    }
                }
            }
            catch (Exception)
            { }
            rizin.Command("fss-");
        }

        private static void ParseMatch(string name, Match match, out decimal offset, out string length, out string identifier, out string mark)
        {
            string soffset = match.Groups[1].Value;
            offset = ulong.Parse(soffset.Substring(2), NumberStyles.HexNumber);
            length = string.Empty;
            if (match.Groups[2].Success)
                length = match.Groups[2].Value.Trim(':');
            if (string.IsNullOrWhiteSpace(length))
                length = "1";

            identifier = string.Empty;
            if (match.Groups[3].Success)
                identifier = match.Groups[3].Value.Replace("$", string.Empty).Trim(':');

            mark = $"loc.yara.{name}";
            if (!string.IsNullOrWhiteSpace(identifier))
                mark = $"{mark}.{identifier}";
        }

        private void GetRawData(string length, decimal? mappedOffset, out string rawdata, out string rawascii)
        {
            using (var json = rizin.CommandJson($"pcj @! {length} @ {mappedOffset}"))
            {
                var root = json.RootElement;
                var len = root.GetArrayLength();
                StringBuilder hex = new StringBuilder(len * 2);
                StringBuilder asc = new StringBuilder(len);
                foreach (byte b in root.EnumerateArray().Select(x => x.GetByte()))
                {
                    hex.AppendFormat("{0:x2}", b);
                    if (b >= 32 && b < 127)
                        asc.Append((char)b);
                    else
                        asc.Append('.');
                }
                rawdata = hex.ToString();
                rawascii = asc.ToString();
            }
        }

        private void MarkInsideRizin(int cnt, string name, decimal offset, string length, string identifier, string mark, decimal? mappedOffset)
        {
            rizin.Command($"f+ '{mark}.{cnt}' {length} 'Yara match \"{name}\" at \"{offset}\" lenght \"{length}\" identifier \"{identifier}\"' @ {mappedOffset}");
        }

        private static void WriteJson(Utf8JsonWriter jsonWriter, decimal offset, string length, string identifier, string rawdata, string rawascii)
        {
            jsonWriter.WriteStartObject();
            jsonWriter.WriteNumber("offset", offset);
            jsonWriter.WriteString("length", length);
            jsonWriter.WriteString("identifier", identifier);
            if (!string.IsNullOrWhiteSpace(rawdata))
            {
                jsonWriter.WriteString("raw", rawdata);
                jsonWriter.WriteString("ascii", rawascii);
            }
            jsonWriter.WriteEndObject();
        }

        private decimal? MapYaraToRizinOffset(decimal offset)
        {
            try
            {
                using (var json = rizin.CommandJson("iSj"))
                {
                    foreach (var elem in json.RootElement.EnumerateArray())
                    {
                        decimal paddr = elem.GetProperty("paddr").GetDecimal();
                        decimal vaddr = elem.GetProperty("vaddr").GetDecimal();
                        decimal psize = elem.GetProperty("size").GetDecimal();
                        if (paddr <= offset && paddr + psize > offset)
                        {
                            decimal mappedOffset = vaddr + offset - paddr;
                            return mappedOffset;
                        }
                    }
                }
            }
            catch (Exception)
            { }
            return null;
        }
    }
}