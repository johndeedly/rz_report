using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.Json;
using toolbelt;

namespace rz_report
{
    public class Data
    {
        Rizin rizin;
        string path, pathData, origName;

        public Data(Rizin rizin, string path, string origName)
        {
            this.rizin = rizin;
            this.path = path;
            this.pathData = $"{path}/data";
            this.origName = origName;
            Directory.CreateDirectory(this.pathData);
        }

        public void Export()
        {
            Sections();
            Resources();
        }

        private DataColumn GetOrAddColumn(DataTable resultTable, string columnName, Type columnType)
        {
            DataColumn col = resultTable.Columns.OfType<DataColumn>().SingleOrDefault(x => x.ColumnName == columnName);
            if (col == null)
                col = resultTable.Columns.Add(columnName, columnType);
            return col;
        }

        private void Sections()
        {
            decimal fileSize = GetMainFileSize();
            MapMainFileToOffsetZero();
            using (DataTable dt = rizin.CommandDataTable("iSj"))
            {
                if (dt == null || dt.Rows.Count == 0)
                    return;

                int index = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string name;
                    decimal paddr, size;
                    if (TryGetValue(dr, ".[].name", out name) &&
                        TryGetValue(dr, ".[].paddr", out paddr) &&
                        TryGetValue(dr, ".[].size", out size))
                    {
                        using (var stream = new FileStream($"{pathData}/sec_{index++}_{name}.bin", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
                        {
                            DumpRangeToFile(paddr, size, stream);
                            stream.Flush();

                            if (size > 0)
                            {
                                stream.Seek(0, SeekOrigin.Begin);
                                double entropy = CalculateEntropy(stream);
                                dr[GetOrAddColumn(dt, ".[].entropy", typeof(decimal))] = (decimal)entropy;

                                stream.Seek(0, SeekOrigin.Begin);
                                string md5 = Hashes.CalculateMD5FromStream(stream);
                                dr[GetOrAddColumn(dt, ".[].md5", typeof(string))] = md5;

                                stream.Seek(0, SeekOrigin.Begin);
                                string sha1 = Hashes.CalculateSha1FromStream(stream);
                                dr[GetOrAddColumn(dt, ".[].sha1", typeof(string))] = sha1;

                                stream.Seek(0, SeekOrigin.Begin);
                                string sha256 = Hashes.CalculateSha256FromStream(stream);
                                dr[GetOrAddColumn(dt, ".[].sha256", typeof(string))] = sha256;
                            }
                        }
                    }
                }

                decimal? overlayOffset = GetOverlayOffset(dt);
                if (overlayOffset.HasValue && overlayOffset < fileSize)
                {
                    using (var stream = new FileStream($"{pathData}/sec_{index++}_overlay.bin", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
                    {
                        decimal size = fileSize - overlayOffset.Value;
                        DumpRangeToFile(overlayOffset.Value, size, stream);
                        stream.Flush();

                        DataRow dr = dt.NewRow();
                        dt.Rows.Add(dr);

                        dr[GetOrAddColumn(dt, ".[].type", typeof(string))] = "OVERLAY";
                        dr[GetOrAddColumn(dt, ".[].size", typeof(decimal))] = size;
                        dr[GetOrAddColumn(dt, ".[].paddr", typeof(string))] = overlayOffset.Value;

                        if (size > 0)
                        {
                            stream.Seek(0, SeekOrigin.Begin);
                            double entropy = CalculateEntropy(stream);
                            dr[GetOrAddColumn(dt, ".[].entropy", typeof(decimal))] = (decimal)entropy;

                            stream.Seek(0, SeekOrigin.Begin);
                            string md5 = Hashes.CalculateMD5FromStream(stream);
                            dr[GetOrAddColumn(dt, ".[].md5", typeof(string))] = md5;

                            stream.Seek(0, SeekOrigin.Begin);
                            string sha1 = Hashes.CalculateSha1FromStream(stream);
                            dr[GetOrAddColumn(dt, ".[].sha1", typeof(string))] = sha1;

                            stream.Seek(0, SeekOrigin.Begin);
                            string sha256 = Hashes.CalculateSha256FromStream(stream);
                            dr[GetOrAddColumn(dt, ".[].sha256", typeof(string))] = sha256;
                        }
                    }
                }

                JsonDocument jdoc;
                using (dt.ToJson(out jdoc))
                using (var stream1 = new FileStream($"{path}/{origName} - sections.json", FileMode.Create, FileAccess.Write, FileShare.Read))
                using (var writer = new Utf8JsonWriter(stream1))
                using (var stream2 = new FileStream($"{path}/{origName} - sections.csv", FileMode.Create, FileAccess.Write, FileShare.Read))
                using (var stream3 = new FileStream($"{path}/{origName} - sections.xlsx", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
                {
                    jdoc.WriteTo(writer);
                    dt.ToCsv(stream2).ToOxml(stream3);
                }
            }
            UnloadMappedFileAtOffsetZero();
        }

        static double CalculateEntropy(Stream s)
        {
            long size = s.Length - s.Position;
            long[] counts = new long[256];
            byte[] buf = new byte[4096];
            int read;
            while ((read = s.Read(buf, 0, buf.Length)) > 0)
            {
                for (int i = 0; i < read; i++)
                {
                    counts[buf[i]]++;
                }
            }

            double entropy = 0;
            for (int i = 0; i < counts.Length; i++)
            {
                if (counts[i] > 0)
                {
                    double probability = (double)counts[i] / size;
                    entropy += probability * Math.Log2(probability);
                }
            }
            return -entropy;
        }

        private static bool TryGetValue<T>(DataRow dr, string col, out T value)
        {
            if (dr[col] == DBNull.Value)
            {
                value = default(T);
                return false;
            }
            value = (T)dr[col];
            return true;
        }

        private decimal GetMainFileSize()
        {
            using (DataTable dt = rizin.CommandDataTable("ij"))
            {
                decimal fileSize = (decimal)dt.Rows[0][".core.size"];
                return fileSize;
            }
        }

        private static decimal? GetOverlayOffset(DataTable dt)
        {
            DataRow lastSection = dt.Rows
                .OfType<DataRow>()
                .Where(x => x[".[].paddr"] != DBNull.Value && x[".[].size"] != DBNull.Value)
                .OrderBy(x => (decimal)x[".[].paddr"])
                .LastOrDefault();
            if (lastSection == null)
                return null;
            decimal lastAddr = (decimal)lastSection[".[].paddr"];
            decimal lastSize = (decimal)lastSection[".[].size"];
            decimal overlayOffset = lastAddr + lastSize;
            return overlayOffset;
        }

        private void UnloadMappedFileAtOffsetZero()
        {
            using (DataTable dt = rizin.CommandDataTable("olj"))
            {
                DataRow last = dt.Rows.OfType<DataRow>().OrderBy(x => (decimal)x[".[].fd"]).Last();
                decimal fd = (decimal)last[".[].fd"];
                rizin.Command($"o- {fd}");
            }
        }

        private void MapMainFileToOffsetZero()
        {
            using (JsonDocument json = rizin.CommandJson("ol.j"))
            {
                string mainFilePath = json.RootElement.GetProperty("uri").GetString();
                rizin.Command($"on \"{mainFilePath}\" 0");
            }
        }

        private void Resources()
        {
            using (DataTable dt = rizin.CommandDataTable("iRj"))
            {
                if (dt == null)
                    return;

                foreach (DataRow dr in dt.Rows)
                {
                    string name = (string)dr[".[].name"];
                    decimal index = (decimal)dr[".[].index"];
                    decimal vaddr = (decimal)dr[".[].vaddr"];
                    decimal size = (decimal)dr[".[].size"];

                    using (var stream = new FileStream($"{pathData}/res_{index}_{name}.bin", FileMode.Create, FileAccess.Write, FileShare.Read))
                    {
                        DumpRangeToFile(vaddr, size, stream);
                        stream.Flush();
                    }
                }
            }
        }

        private void DumpRangeToFile(decimal vaddr, decimal size, Stream stream)
        {
            using (JsonDocument json = rizin.CommandJson($"pxj {size} @ {vaddr}"))
            {
                int length = json.RootElement.GetArrayLength();
                byte[] data = new byte[length];
                int i = 0;
                foreach (var elem in json.RootElement.EnumerateArray())
                {
                    data[i++] = elem.GetByte();
                }
                stream.Write(data, 0, length);
            }
        }
    }
}