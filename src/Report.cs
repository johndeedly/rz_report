using System;
using System.Buffers;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using toolbelt;

namespace rz_report
{
    public class Report
    {
        Rizin rizin;
        string basePath;
        string origName;

        public Report(Rizin rizin, string basePath, string origName)
        {
            this.rizin = rizin;
            this.basePath = basePath;
            this.origName = origName;
        }

        private void Generate(string cmd, string fileName, bool yx = false)
        {
            using (var json = rizin.CommandJson(cmd))
            {
                Generate(json, fileName, yx);
            }
        }

        private void Generate(JsonDocument json, string fileName, bool yx = false)
        {
            if (json == null)
                return;
            using (var stream = new FileStream($"{basePath}/{origName} - {fileName}.json", FileMode.Create, FileAccess.Write, FileShare.Read))
            using (var writer = new Utf8JsonWriter(stream))
            {
                json.WriteTo(writer);
            }
            using (var stream1 = new FileStream($"{basePath}/{origName} - {fileName}.csv", FileMode.Create, FileAccess.Write, FileShare.Read))
            using (var stream2 = new FileStream($"{basePath}/{origName} - {fileName}.xlsx", FileMode.Create, FileAccess.ReadWrite, FileShare.Read))
            using (DataTable dt = new DataTable())
            {
                dt.FromJson(json.RootElement);
                if (yx)
                    dt.ToCsvTransposed(stream1).ToOxmlTransposed(stream2);
                else
                    dt.ToCsv(stream1).ToOxml(stream2);
            }
        }

        public void Hashes()
        {
            var outputBuffer = new ArrayBufferWriter<byte>();
            using (var jsonWriter = new Utf8JsonWriter(outputBuffer))
            {
                jsonWriter.WriteStartObject();
                using (var json = rizin.CommandJson("itj"))
                {
                    foreach (var elem in json.RootElement.EnumerateObject())
                    {
                        elem.WriteTo(jsonWriter);
                    }
                }
                TryAddSsdeepHash(jsonWriter);
                jsonWriter.WriteEndObject();
            }
            using (var json = JsonDocument.Parse(Encoding.UTF8.GetString(outputBuffer.WrittenSpan)))
            {
                Generate(json, "hashes", true);
            }
        }

        private bool TryAddSsdeepHash(Utf8JsonWriter jsonWriter)
        {
            try
            {
                string filePath = null;
                using (var json = rizin.CommandJson("ij"))
                {
                    filePath = json.RootElement.GetProperty("core").GetProperty("file").GetString();
                }
                if (!string.IsNullOrWhiteSpace(filePath))
                {
                    string data = ShellUtils.RunShellTextAsync("ssdeep", $"-c \"{filePath}\"")
                        .GetAwaiter()
                        .GetResult();
                    string hash = data.Split('\n')[1].Split(',').First();
                    jsonWriter.WriteString("ssdeep", hash);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public void Info()
        {
            Generate("ij", "info", true);
        }

        public void Entrypoints()
        {
            var outputBuffer = new ArrayBufferWriter<byte>();
            using (var json1 = rizin.CommandJson("iej"))
            using (var json2 = rizin.CommandJson("ieej"))
            using (var jsonWriter = new Utf8JsonWriter(outputBuffer))
            {
                jsonWriter.WriteStartArray();
                if (json1 != null)
                {
                    foreach (var elem in json1.RootElement.EnumerateArray())
                    {
                        elem.WriteTo(jsonWriter);
                    }
                }
                if (json2 != null)
                {
                    foreach (var elem in json2.RootElement.EnumerateArray())
                    {
                        elem.WriteTo(jsonWriter);
                    }
                }
                jsonWriter.WriteEndArray();
            }
            using (var json = JsonDocument.Parse(Encoding.UTF8.GetString(outputBuffer.WrittenSpan)))
            {
                Generate(json, "entrypoints");
            }
        }

        public void Headers()
        {
            Generate("ihj", "headers");
            var outputBuffer = new ArrayBufferWriter<byte>();
            using (var json = rizin.CommandJson("ihj"))
            using (var jsonWriter = new Utf8JsonWriter(outputBuffer))
            {
                jsonWriter.WriteStartArray();
                try
                {
                    string linkerRich = GetHeaderValue(json, "comment", "Linker");
                    string linkerMajorHeader = GetHeaderValue(json, "name", "MajorLinkerVersion");
                    string linkerMinorHeader = GetHeaderValue(json, "name", "MinorLinkerVersion");
                    if (linkerRich != null && linkerMajorHeader != null && linkerMinorHeader != null)
                    {
                        int tmp = int.Parse(Regex.Match(linkerRich, @"\d+").Value);
                        int linkerMajorRichVal = tmp / 100;
                        int linkerMinorRichVal = tmp % 100;
                        int linkerMajorHeaderVal = int.Parse(linkerMajorHeader.Substring(2), NumberStyles.AllowHexSpecifier);
                        int linkerMinorHeaderVal = int.Parse(linkerMinorHeader.Substring(2), NumberStyles.AllowHexSpecifier);

                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteString("name", "AnalyzeRichLinkerVersion");
                        jsonWriter.WriteString("comment", $"{linkerMajorRichVal}.{linkerMinorRichVal}");
                        jsonWriter.WriteEndObject();
                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteString("name", "AnalyzeHeaderLinkerVersion");
                        jsonWriter.WriteString("comment", $"{linkerMajorHeaderVal}.{linkerMinorHeaderVal}");
                        jsonWriter.WriteEndObject();
                        if (linkerMajorRichVal != linkerMajorHeaderVal ||
                            linkerMinorRichVal != linkerMinorHeaderVal)
                        {
                            jsonWriter.WriteStartObject();
                            jsonWriter.WriteString("name", "AnalyzeRichHeaderTampering");
                            jsonWriter.WriteString("comment", "Linker version mismatch");
                            jsonWriter.WriteEndObject();
                        }
                    }
                    IEnumerable<string> dup = CheckDuplicateRichElement(json);
                    foreach (var elem in dup)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WriteString("name", "AnalyzeRichHeaderTampering");
                        jsonWriter.WriteString("comment", $"Duplicate entry {elem}");
                        jsonWriter.WriteEndObject();
                    }
                }
                catch (Exception)
                { }
                foreach (var elem in json.RootElement.EnumerateArray())
                {
                    elem.WriteTo(jsonWriter);
                }
                jsonWriter.WriteEndArray();
            }
            using (var json = JsonDocument.Parse(Encoding.UTF8.GetString(outputBuffer.WrittenSpan)))
            {
                Generate(json, "headers");
            }
        }

        private static string GetHeaderValue(JsonDocument json, string property, string contains)
        {
            string val = null;
            foreach (var elem in json.RootElement.EnumerateArray())
            {
                JsonElement prop;
                if (elem.TryGetProperty(property, out prop))
                {
                    string chck = prop.GetString();
                    if (contains == null || chck.StartsWith(contains))
                    {
                        if (elem.TryGetProperty("comment", out prop))
                        {
                            val = prop.GetString();
                        }
                    }
                }
            }
            return val;
        }

        private static IEnumerable<string> CheckDuplicateRichElement(JsonDocument json)
        {
            HashSet<string> val = new HashSet<string>();
            HashSet<string> dup = new HashSet<string>();
            foreach (var elem in json.RootElement.EnumerateArray())
            {
                JsonElement prop;
                if (elem.TryGetProperty("name", out prop))
                {
                    string chck = prop.GetString();
                    if (chck == "RICH_ENTRY_NAME")
                    {
                        if (elem.TryGetProperty("comment", out prop))
                        {
                            if (!val.Add(prop.GetString()))
                            {
                                dup.Add(prop.GetString());
                            }
                        }
                    }
                }
            }
            return dup;
        }

        // public void Sections()
        // {
        //     Generate("iSj entropy,md5,sha1,sha256", "sections");
        // }

        public void Resources()
        {
            Generate("iRj", "resources");
        }

        public void Libraries()
        {
            Generate("ilj", "libraries");
        }

        public void Imports()
        {
            Generate("iij", "imports");
        }

        public void Exports()
        {
            Generate("iEj", "exports");
        }

        public void Signature()
        {
            Generate("iCj", "signature");
        }

        public void Strings()
        {
            Generate("izj", "strings");
        }

        public void Functions()
        {
            Generate("aflj", "functions");
        }

        private JsonDocument GetClasses()
        {
            JsonDocument jsonCls = null;
            try
            {
                jsonCls = rizin.CommandJson("aclj");
            }
            catch (Exception)
            {
                Console.WriteLine("error retrieving classes");
            }
            if (jsonCls == null)
                return null;

            int clsCnt = jsonCls.RootElement.GetArrayLength();
            Console.WriteLine($"analyzing {clsCnt} classes");

            int i = 0;
            foreach (var cls in jsonCls.RootElement.EnumerateArray())
            {
                i++;
                try
                {
                    string name = cls.GetProperty("name").GetString();

                    JsonElement meths;
                    if (cls.TryGetProperty("methods", out meths))
                    {
                        foreach (var meth in meths.EnumerateArray())
                        {
                            string methname = meth.GetProperty("name").GetString();
                            decimal addr = meth.GetProperty("addr").GetDecimal();
                            Console.WriteLine($"[{i}/{clsCnt}] analyzing {name}.{methname} @ {addr}");
                            rizin.Command($"af @ {addr}");
                        }
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("error analyzing class");
                }
            }
            return jsonCls;
        }

        public void Classes()
        {
            using (JsonDocument json = GetClasses())
            {
                Generate(json, "classes");
            }
        }

        private static string backslash = "\\";
        private static string doublebackslash = "\\\\";
        private static string mark = "'";
        private static string markescaped = "\\'";

        private JsonDocument GetStackStringsJson()
        {
            rizin.Command("fs stackstrings");
            rizin.Command("fss+ stackstrings");
            
            var outputBuffer = new ArrayBufferWriter<byte>();
            int cnt = 0;
            using (var jsonWriter = new Utf8JsonWriter(outputBuffer))
            {
                jsonWriter.WriteStartArray();

                using (JsonDocument jsonFcn = rizin.CommandJson("aflj"))
                {
                    if (jsonFcn == null)
                        return null;

                    foreach (var fcn in jsonFcn.RootElement.EnumerateArray())
                    {
                        decimal offset = fcn.GetProperty("offset").GetDecimal();
                        string name = fcn.GetProperty("name").GetString();
                        bool firstString = true;

                        using (JsonDocument jsonInst = rizin.CommandJson($"pifj @ {offset}"))
                        {
                            if (jsonInst == null)
                                continue;
                            foreach (var inst in jsonInst.RootElement.GetProperty("ops").EnumerateArray())
                            {
                                JsonElement valElem, tmpElem;
                                if (!inst.TryGetProperty("val", out valElem))
                                    continue;
                                if (inst.TryGetProperty("refs", out tmpElem) && tmpElem.GetArrayLength() > 0)
                                    continue;
                                decimal opoffset = inst.GetProperty("offset").GetDecimal();
                                decimal size = inst.GetProperty("size").GetDecimal();
                                decimal val = valElem.GetDecimal();
                                string opcode = inst.GetProperty("opcode").GetString();
                                string type = inst.GetProperty("type").GetString();
                                if (type != "mov" && type != "push")
                                    continue;
                                byte[] data = BitConverter.GetBytes((ulong)val);
                                List<char> chars = new List<char>();
                                for (int i = 0; i < data.Length; i++)
                                {
                                    if (data[i] >= 0x20 && data[i] <= 0x7A)
                                    {
                                        char chr = (char)data[i];
                                        chars.Add(chr);
                                    }
                                }
                                if (chars.Count > 0)
                                {
                                    string str = new string(chars.ToArray());
                                    string strComm = new string(chars.ToArray());
                                    if (strComm.Contains('\\'))
                                    {
                                        strComm = strComm.Replace(backslash, doublebackslash);
                                    }
                                    if (strComm.Contains('\''))
                                    {
                                        strComm = strComm.Replace(mark, markescaped);
                                    }

                                    rizin.Command($"f 'loc.stackstring.{cnt}' {size} '{strComm}' @ {opoffset}");
                                    cnt++;

                                    if (firstString)
                                    {
                                        jsonWriter.WriteStartObject();
                                        jsonWriter.WriteNumber("offset", offset);
                                        jsonWriter.WriteString("name", name);
                                        jsonWriter.WriteStartArray("ops");
                                        firstString = false;
                                    }
                                    jsonWriter.WriteStartObject();
                                    jsonWriter.WriteString("string", str);
                                    jsonWriter.WriteNumber("offset", opoffset);
                                    jsonWriter.WriteNumber("val", val);
                                    jsonWriter.WriteString("opcode", opcode);
                                    jsonWriter.WriteEndObject();
                                }
                            }
                        }
                        if (!firstString)
                        {
                            jsonWriter.WriteEndArray();
                            jsonWriter.WriteEndObject();
                        }
                    }
                }

                jsonWriter.WriteEndArray();
            }
            rizin.Command("fss-");
            return JsonDocument.Parse(Encoding.UTF8.GetString(outputBuffer.WrittenSpan));
        }

        public void StackStrings()
        {
            using (JsonDocument json = GetStackStringsJson())
            {
                Generate(json, "stackstrings");
            }
        }
    }
}