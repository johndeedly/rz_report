﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using ICSharpCode.Decompiler;
using ICSharpCode.Decompiler.CSharp;
using toolbelt;

namespace rz_report
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 1)
            {
                Parallel.ForEach(args, new ParallelOptions
                {
                    MaxDegreeOfParallelism = Environment.ProcessorCount
                }, (arg) => ShellUtils.RunChildAsync(arg).Await());
            }
            else if (args.Length == 1)
            {
                string arg = args[0];
                if (File.Exists(arg))
                {
                    string hash = Hashes.CalculateMD5FromStream(File.OpenRead(arg));
                    
                    using (var rizin = new Rizin())
                    {
                        rizin.Command($"o \"{arg}\"");
                        string origName = Path.GetFileName(arg);
                        string fileName = $"{hash} - {origName}";

                        string path = $"rz_report/{fileName}";
                        if (Directory.Exists(path) && !Debugger.IsAttached)
                        {
                            Console.Error.WriteLine($"path already exists ({path}).");
                            return;
                        }
                            
                        Directory.CreateDirectory(path);

                        Report report = new Report(rizin, path, origName);

                        rizin.CommandAnalyzeBinary();

                        Yara yara = new Yara(rizin, path, origName);
                        yara.TryYaraCheck();

                        report.Hashes();
                        report.Info();

                        if (CheckIsNotExecutable(rizin))
                            return;

                        if (CheckIsCilExecutable(rizin))
                        {
                            try
                            {
                                var decompiler = new CSharpDecompiler(arg, new DecompilerSettings());
                                string code = decompiler.DecompileWholeModuleAsString();
                                string cilFileName = string.Concat(path, Path.DirectorySeparatorChar, $"{origName} - cil.txt");
                                File.WriteAllText(cilFileName, code);
                            }
                            catch (Exception)
                            { }
                            return;
                        }

                        report.Headers();
                        //report.Sections();
                        report.Resources();
                        report.Libraries();
                        report.Signature();
                        report.Entrypoints();
                        report.Imports();
                        report.Exports();
                        report.Classes();
                        report.Strings();
                        report.StackStrings();
                        report.Functions();

                        rizin.Command($"Ps \"{path}/{origName} - project.rzdb\"");

                        Data data = new Data(rizin, path, origName);
                        data.Export();

                        //Opcodes opcodes = new Opcodes(rizin, path);
                        //opcodes.Disassemble();
                    }
                }
            }
            else
            {
                Console.WriteLine(@"usage: rz_report [FILE]...");
                Console.WriteLine(@"creates a folder ""./rz_report"" and writes the analysis " +
                                  @"results inside subfolders named after their MD5 sums.");
            }
        }

        private static bool CheckIsNotExecutable(Rizin rizin)
        {
            using (var json = rizin.CommandJson("aflj"))
            {
                if (json == null || json.RootElement.GetArrayLength() < 1)
                    return true;
            }
            return false;
        }

        private static bool CheckIsCilExecutable(Rizin rizin)
        {
            using (var json = rizin.CommandJson("ij"))
            {
                if (json != null)
                {
                    JsonElement elem = json.RootElement;
                    if (elem.TryGetProperty("bin", out elem))
                    {
                        if (elem.TryGetProperty("lang", out elem))
                        {
                            if (elem.GetString() == "cil")
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }
    }
}
